require('dotenv').config()

const jwtConfig = {

  SECRET_KEY: `${process.env.SECRET_KEY}` || 'the-secret-key',

  TOKEN_TTL: `${process.env.TOKEN_TTL}` || '15s'

}

module.exports = jwtConfig
