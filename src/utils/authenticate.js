const getAccessToken = () => localStorage.getItem('token')

const deleteAccessToken = () => localStorage.removeItem('token')

const setAccessToken = (token) => localStorage.setItem('token', token)

const hasAccessToken = () => getAccessToken() !== null

export { getAccessToken, deleteAccessToken, setAccessToken, hasAccessToken }
