import * as actionTypes from '../const/actionTypes'

/**
 * Credentials includes name / email
 */

const actions = {
  async setUserData ({ commit }, credentials) {
    try {
      commit(actionTypes.SET_USER_DATA, credentials)
    } catch (e) {
      console.error(e)
    }
  }
}

export default actions
