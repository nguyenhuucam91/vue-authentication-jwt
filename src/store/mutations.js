import * as actionTypes from '../const/actionTypes'

const mutations = {
  [actionTypes.SET_USER_DATA]: (state, credentials) => {
    state.user = credentials
  }
}

export default mutations
