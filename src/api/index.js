import axios from 'axios'
import { getAccessToken, setAccessToken } from '../utils/authenticate'

const instance = axios.create({
  baseURL: '//localhost:3000/'
})

instance.interceptors.request.use(config => {
  const accessToken = getAccessToken()
  if (accessToken) {
    config.headers.common['Authorization'] = `Bearer ${accessToken}`
  }
  config.headers.common['Content-Type'] = 'application/json'
  return config
}, error => {
  console.error(error)
})

instance.interceptors.response.use(response => {
  return response
}, error => {
  const originalRequest = error.config
  if (error.response.status === 401 && error.response.data.message === 'Token expired' && !originalRequest._retry) {
    originalRequest._retry = true
    // call refresh
    return axios({
      baseURL: '//localhost:3000/',
      url: 'refresh',
      method: 'POST'
    }).then(res => {
      setAccessToken(res.data.token)
      originalRequest.headers['Authorization'] = `Bearer ${res.data.token}`
      return instance(originalRequest)
    })
  }
  return Promise.reject(error)
})

const request = {
  get: (url, options) => {
    return instance({
      url,
      method: 'GET',
      ...options
    })
  },

  post: (url, data, options) => {
    return instance({
      url,
      method: 'POST',
      data,
      ...options
    })
  }
}

const removeAuthorizationHeader = () => {
  delete instance.defaults.headers['Authorization']
}

export { request, removeAuthorizationHeader }
