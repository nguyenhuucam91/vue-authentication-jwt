import store from '../store/index'

export default async (to, from, next) => {
  if (store.state.user !== null) {
    next({ name: 'dashboard' })
  }
  next()
}
