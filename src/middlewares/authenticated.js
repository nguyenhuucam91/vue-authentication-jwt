import store from '../store/index'

export default (to, from, next) => {
  const res = store.getters.user
  if (res === null) {
    next({ name: 'login' })
  }
  next()
}
