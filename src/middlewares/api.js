import { request } from '../api/index'
import { hasAccessToken } from '../utils/authenticate'
import * as actionTypes from '../const/actionTypes'
import store from '../store/index'

export default async (to, from, next) => {
  if (hasAccessToken() && store.getters.user === null) {
    const res = await request.get('/user')
    const userInfo = res.data.userInfo
    store.commit(actionTypes.SET_USER_DATA, {
      name: userInfo.name,
      email: userInfo.email
    })
  }
  next()
}
