import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Dashboard from './views/Dashboard.vue'
import Register from './views/Register.vue'
import Login from './views/Login.vue'
import authenticated from './middlewares/authenticated'
import api from './middlewares/api'
import multiguard from 'vue-router-multiguard'
import guest from './middlewares/guest'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      beforeEnter: multiguard([authenticated])
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      beforeEnter: multiguard([guest])
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      beforeEnter: multiguard([guest])
    }
  ]
})

router.beforeEach(api)

export default router
