const express = require('express')
const jwt = require('jsonwebtoken')
const cors = require('cors')
const bodyParser = require('body-parser')
const fs = require('fs')
const events = require('./db/events.json')
const app = express()
const jwtConfig = require('./config/jwt')

app.use(cors())
app.use(bodyParser.json())

const secretKey = jwtConfig.SECRET_KEY
const tokenTTL = jwtConfig.TOKEN_TTL

app.get('/', (req, res) => {
  res.json({
    message: 'Welcome to the API.'
  })
})

app.get('/dashboard', verifyToken, (req, res) => {
  res.json({
    events: events
  })
})

app.post('/register', (req, res) => {
  console.log(req.body)
  if (req.body) {
    const user = {
      name: req.body.name,
      email: req.body.email,
      password: req.body.password
      // In a production app, you'll want to encrypt the password
    }

    const data = JSON.stringify(user, null, 2)
    var dbUserEmail = require('./db/user.json').email
    if (dbUserEmail === req.body.email) {
      res.status(400).json({ message: 'Email exist' })
    } else {
      fs.writeFile('./db/user.json', data, err => {
        if (err) {
          console.log(err + data)
        } else {
          const token = jwt.sign({
            userInfo: user
          }, secretKey, { expiresIn: tokenTTL })
          // In a production app, you'll want the secret key to be an environment variable
          res.json({
            token,
            expiresIn: tokenTTL
          })
        }
      })
    }
  } else {
    res.sendStatus(400)
  }
})

app.post('/login', (req, res) => {
  const userDB = fs.readFileSync('./db/user.json')
  const userInfo = JSON.parse(userDB)
  if (
    req.body &&
    req.body.email === userInfo.email &&
    req.body.password === userInfo.password
  ) {
    const token = jwt.sign({ userInfo }, secretKey, { expiresIn: tokenTTL })
    // In a production app, you'll want the secret key to be an environment variable
    res.json({
      token,
      expiresIn: tokenTTL
    })
  } else {
    res.status(401).json({ message: 'Invalid credential' })
  }
})

app.get('/user', verifyToken, (req, res) => {
  return res.json(res.decoded)
})

app.post('/refresh', (req, res) => {
  const userDB = fs.readFileSync('./db/user.json')
  const userInfo = JSON.parse(userDB)
  const token = jwt.sign({
    userInfo
  }, secretKey, { expiresIn: tokenTTL })
  res.json({
    token,
    expiresIn: tokenTTL
  })
})

// MIDDLEWARE
function verifyToken (req, res, next) {
  try {
    const authorizationHeader = req.headers['authorization']
    const authorizationHeaderFragment = authorizationHeader.split(' ')
    const token = authorizationHeaderFragment[1]
    if (token) {
      const decoded = jwt.verify(token, secretKey)
      res.decoded = decoded
      next()
    }
  } catch (e) {
    res.status(401).send({ message: 'Token expired' })
  }
}

app.listen(3000, () => {
  console.log('Server started on port 3000')
})
