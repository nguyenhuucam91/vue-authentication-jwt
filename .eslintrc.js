module.exports = {
  root: true,
  env: {
    node: true
  },
  plugins: ['jest'],
  extends: ["plugin:vue/recommended", "standard", "plugin:jest/recommended"],
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    "vue/max-attributes-per-line": ["error", {
      "singleline": 4,
      "multiline": {
        "max": 1,
        "allowFirstLine": false
      }
    }],
  },
  parserOptions: {
    parser: "babel-eslint"
  },
  overrides: [
    {
      "files": [
        "**/*.spec.js",
        "**/*.spec.jsx"
      ],
      "env": {
        "jest": true
      }
    }
  ]
};
